<?php

// Total  8 Data types

        // 4 Data Types are Scalar Types

    // Boolean
    $myTestVar = true;
    var_dump($myTestVar);
    echo "<br>";

    // integer
    $myTestVar = 64;
    var_dump($myTestVar);
    echo "<br>";

    // Float
    $myTestVar = 6.9;
    var_dump($myTestVar);
    echo "<br>";

    $testVariable = 20000;

    //String
    $myTestVar = 'This is single Quoted String - $testVariable';
    echo "<br>";
    var_dump($myTestVar);
    echo "<br>";

    // Double Quoted String
    echo $myTestVar = "This is Double Quoted String - $testVariable";
    echo "<br>";
    var_dump($myTestVar);
    echo "<br><br>";

    //Nowdoc
        $myTestVar =<<<'Hello'
        sting inside Nowdoc string<br>
            sting inside \t \t Nowdoc string 1234<br>
            sting "inside" Nowdoc 'string' $testVariable
Hello;

    echo "$myTestVar<br><br>";

//Heredoc
$myTestVar =<<<Hello
        sting inside heredoc string<br>
            sting inside heredoc string 1234<br>
            sting inside \t \t \n heredoc string "group" 'number' $testVariable
Hello;

echo "$myTestVar<br>";
$person = array(
    "Sayem"=>array("Sayem Hossain",24,5.9),
    "Rakib"=>array("Rakib Hossain",40,6.0),
    "Shaiful"=>array("Shaiful Kabir",26)
    );
echo "<br>";
foreach($person as $nick=>$personInfo){
    echo "$nick:<br>";
    foreach($personInfo as $key=>$value) {
        echo " $key: ".$value."<br>";
    }
    echo "<br>";
}

